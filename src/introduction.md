# Introduction

**menoci** is a collection of Drupal extension modules that can be used to rapidly deploy an integrated website 
and data management portal for research projects in the biomedical and life sciences. It is a featured tool from the [Tool Pool](https://www.toolpool-gesundheitsforschung.de/produkte/menociio-lightweight-data-management-biomedical-research) organised by [TMF](https://www.tmf-ev.de/home.aspx).

The menoci modules are actively developed and maintained by the University Medical Center Göttingen, 
[Department of Medical Informatics](http://mi.umg.eu), and Göttingen Scientific Data Center, [GWDG](https://gwdg.de).

## menoci Source Code

The menoci suite is distributed via the dedicated [menoci GitLab repository](https://gitlab.gwdg.de/medinfpub/menoci/).

### Original Repositories

![](img/module_icons_1200px.png)

This distribution repository comprises the following menoci modules:

* [Commons](https://gitlab.gwdg.de/research-data-platform/sfb-commons):
 shared libraries, group and subproject handling
* [Data Archive](https://gitlab.gwdg.de/research-data-platform/rdp-archive):
 store and share data packages (using 
[CDSTAR](https://cdstar.gwdg.de) as a backend storage service; 
https://gitlab.gwdg.de/cdstar/cdstar)
* [Literature](https://gitlab.gwdg.de/research-data-platform/sfb-literature): 
 publication list
* [Wikidata](https://gitlab.gwdg.de/research-data-platform/rdp-wikidata):
 optional extension of the publication list, 
 that cross-references Wikidata pages of registered articles
* [Antibody Catalogue](https://gitlab.gwdg.de/research-data-platform/sfb-antibody): 
 track antibodies used in the project
* [Mouse Line Catalogue](https://gitlab.gwdg.de/research-data-platform/sfb-mouseline):
 track mouse lines and specimen 
 used in the project

## License

### Website/Documentation

This documentation is created with [mdbook](https://github.com/azerupi/mdBook).

The source code of this website is available at [menoci-docs GitLab Repository](https://gitlab.gwdg.de/research-data-platform/menoci-docs) 

### menoci Project
Copyright (C) 2012-2020 menoci contributors, see [AUTHORS.md](https://gitlab.gwdg.de/medinfpub/menoci/blob/master/AUTHORS.md)

The menoci project source code is licensed 
under [GNU General Public License 3.0](https://spdx.org/licenses/GPL-3.0-or-later.html), see [license file](https://gitlab.gwdg.de/medinfpub/menoci/blob/master/LICENSE)

### Documentation

This documentation is licensed under Creative Commons license [CC-BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/) unless otherwise stated.

[![Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png "Creative Commons Attribution-ShareAlike 4.0 International License")](http://creativecommons.org/licenses/by-sa/4.0/)

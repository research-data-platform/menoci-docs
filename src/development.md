# Development

Development of _menoci_ is actively pursued at the Göttingen Department of 
Medical Informatics. Please find below a rough outline of release versions and 
latest released features.

The _menoci_ project is subject to change. If you would like to see certain 
features implemented on schedule, feel free to 
[contact us and contribute to development](https://gitlab.gwdg.de/medinfpub/menoci/#contribute).

### WIP 
* **API and swagger documentation:** To extend the accessibility of research data stored in menoci the [Swagger-UI](https://swagger.io/tools/swagger-ui/) documentation for the [menoci API](https://api.menoci.io/) has been added to the project.
* **Demo instance:** menoci and can be tried out and tested with the [demo instance](https://demo.menoci.io/). This instance is free to use and publicly available and will be resetted each night.

[![Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png "Creative Commons Attribution-ShareAlike 4.0 International License")](http://creativecommons.org/licenses/by-sa/4.0/)

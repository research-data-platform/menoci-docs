# menoci Documentation mdbook

* Documentation for the menoci project
* created with [mdbook](https://github.com/azerupi/mdBook)

## Usage

* Clone repository
* `docker-compose up` (Starts a Docker container using the [hrektts/mdbook](https://hub.docker.com/r/hrektts/mdbook) image)
* The documentation page is server at [localhost](http://localhost)

## License

This documentation is licensed under Creative Commons license [CC-BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/) unless otherwise stated.

[![Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png "Creative Commons Attribution-ShareAlike 4.0 International License")](http://creativecommons.org/licenses/by-sa/4.0/)
